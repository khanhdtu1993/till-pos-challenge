export class Pizza {
    constructor(
        public size: 'S' | 'M' | 'L',
        public quantity: number,
        public price?: number
    ) {}
}