interface PricingRule {
    promotionType: 'BOGOF' | 'PARITY'
    ofSizes: ('S' | 'M' | 'L')[]
    buy?: number
    for?: number
    price?: number
}

export class Promotion {
    price: number
    constructor(
        public id: string,
        public pricingRules: PricingRule,
        public forProviders: ('AMAZON' | 'GOOGLE' | 'MICROSOFT')[],
    ) {}
}