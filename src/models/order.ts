import { Customer } from './customer';
import { Pizza } from './pizza';
import { Promotion } from './promotion';

export class Order {
    constructor(
        public id: string,
        public customer: Customer,
        public ordered: Pizza[],
        public remain: Pizza[]
    ) {}

    checkout(promotions: Promotion[]): Checkout {
        let promotionFound: Promotion = null
        let totalS = 0
        let totalBonusS = 0
        let totalM = 0
        let totalBonusM = 0
        let totalL = 0
        let totalBonusL = 0
        let totalPrice = 0

        promotions.map(promotion => {
            if (promotion.forProviders.includes(this.customer.provider)) {
                promotionFound = promotion
                return;
            }
        })
        if (promotionFound && promotionFound.pricingRules.promotionType === 'BOGOF') {
            this.ordered.map(order => {
                if (order.size === 'S') {
                    totalS += order.quantity
                    totalPrice += order.quantity * this.remain[0].price
                }
                if (order.size === 'M') {
                    totalM += order.quantity
                    totalPrice += order.quantity * this.remain[1].price
                }
                if (order.size === 'L') {
                    totalL += order.quantity
                    totalPrice += order.quantity * this.remain[2].price
                }
            })
            if (promotionFound.pricingRules.ofSizes.includes('S')) {
                totalBonusS = Math.floor(totalS / promotionFound.pricingRules.buy) * promotionFound.pricingRules.for
            }
            if (promotionFound.pricingRules.ofSizes.includes('M')) {
                totalBonusM = Math.floor(totalM / promotionFound.pricingRules.buy) * promotionFound.pricingRules.for
            }
            if (promotionFound.pricingRules.ofSizes.includes('L')) {
                totalBonusL = Math.floor(totalL / promotionFound.pricingRules.buy) * promotionFound.pricingRules.for
            }
        }
        if (promotionFound && promotionFound.pricingRules.promotionType === 'PARITY') {
            this.ordered.map(order => {
                if (order.size === 'S') {
                    totalS += order.quantity
                    totalPrice += order.quantity * promotionFound.pricingRules.price
                }
                if (order.size === 'M') {
                    totalM += order.quantity
                    totalPrice += order.quantity * promotionFound.pricingRules.price
                }
                if (order.size === 'L') {
                    totalL += order.quantity
                    totalPrice += order.quantity * promotionFound.pricingRules.price
                }
            })
        }
        return {
            customerName: this.customer.name,
            provider: this.customer.provider,
            totalS, totalBonusS, totalM, totalBonusM, totalL, totalBonusL, totalPrice,
            createdAt: new Date().toString()
        }
    }
}

export interface Checkout {
    customerName: string
    provider: string
    totalS: number
    totalBonusS: number
    totalM: number
    totalBonusM: number
    totalL: number
    totalBonusL: number
    totalPrice: number
    createdAt?: string
}