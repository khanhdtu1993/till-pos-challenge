export class Customer {
    constructor(
        public id: string,
        public name: string,
        public provider: 'AMAZON' | 'GOOGLE' | 'MICROSOFT',
    ) {}
}