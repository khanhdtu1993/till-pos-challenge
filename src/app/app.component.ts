import { Component } from '@angular/core';
import { Checkout, Promotion } from 'src/models';
import { Customer } from '../models/customer';
import { Pizza } from "../models/pizza";
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'till-pos-challenge';
  customers: Customer[];
  pizzas: Pizza[];
  promotions: Promotion[];
  checkouts: Checkout[];
  currentCustomerId: string;
  s = 0;
  m = 0;
  l = 0;

  constructor(public service: AppService) {
    this.service.getPizzas().subscribe(pizzas => {
      this.pizzas = pizzas
      console.log(pizzas)
    })
    this.service.getCustomers().subscribe(customers => {
      this.customers = customers
      console.log(customers)
    })
    this.service.getPromotions().subscribe(promotions => {
      this.promotions = promotions
      console.log(this.promotions)
    })
    this.service.getCheckouts().subscribe(checkouts => {
      this.checkouts = checkouts
      console.log(checkouts)
    })
    this.currentCustomerId = this.service.currentCustomer.id;
  }

  checkout() {
    this.service.checkout(this.s, this.m, this.l)
    this.s = 0
    this.m = 0
    this.l = 0
  }

  updateCustomer(id) {
    this.service.setCurrentCustomer(id)
  }
}
