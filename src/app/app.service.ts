import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Customer, Pizza, Promotion, Order, Checkout } from '../models';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  pizzas = new BehaviorSubject<Pizza[]>([])
  customers = new BehaviorSubject<Customer[]>([])
  promotions = new BehaviorSubject<Promotion[]>([])
  checkouts = new BehaviorSubject<Checkout[]>([])
  currentCustomer: Customer;

  constructor() {
    this.initAvailablePizzas()
    this.initCustomers()
    this.initPromotions()
  }

  initAvailablePizzas() {
    this.pizzas.next([
      new Pizza('S', 300, 10),
      new Pizza('M', 200, 20),
      new Pizza('L', 100, 30),
    ])
  }

  initCustomers() {
    this.customers.next([
      new Customer('01', 'Ronaldo', 'AMAZON'),
      new Customer('02', 'Messi', 'GOOGLE'),
      new Customer('03', 'Neymar', 'MICROSOFT'),
    ])
    this.currentCustomer = this.customers.getValue()[0]
  }

  initPromotions() {
    this.promotions.next([
      new Promotion(
        '01',
        {promotionType: 'BOGOF', ofSizes: ['S','M'], buy: 3, for: 2 },
        ['AMAZON', 'GOOGLE']
      ),

      new Promotion(
        '02',
        {promotionType: 'PARITY', ofSizes: ['L'], price: 20},
        ['MICROSOFT']
      )
    ])
  }

  checkout(s: number, m: number, l: number) {
    const order = new Order(
      new Date().toString(),
      this.currentCustomer,
      [
        {
          size: 'S',
          quantity: s,
        },
        {
          size: 'M',
          quantity: m,
        },
        {
          size: 'L',
          quantity: l,
        }
      ],
      this.pizzas.getValue(),
    )

    const checkout = order.checkout(this.promotions.getValue())

    // add to checkouts list
    this.checkouts.next([
      ...this.checkouts.getValue(),
      checkout
    ])

    // update remain quantity
    this.updateRemainQuantity(checkout)
  }

  getPizzas() {
    return this.pizzas.asObservable()
  }

  getPromotions() {
    return this.promotions.asObservable()
  }

  getCheckouts() {
    return this.checkouts.asObservable()
  }

  updateRemainQuantity(checkout: Checkout) {
    const s = checkout.totalS + checkout.totalBonusS
    const m = checkout.totalM + checkout.totalBonusM
    const l = checkout.totalL + checkout.totalBonusL
    const pizzas = this.pizzas.getValue()
    pizzas[0].quantity = pizzas[0].quantity - s
    pizzas[1].quantity = pizzas[1].quantity - m
    pizzas[2].quantity = pizzas[2].quantity - l
    this.pizzas.next(pizzas)
  }

  getCustomers() {
    return this.customers.asObservable()
  }

  setCurrentCustomer(id: string) {
    this.currentCustomer = this.customers.getValue().find(e => e.id === id)
  }
}
